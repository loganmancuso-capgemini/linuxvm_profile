# linuxvm_profile

` git clone git@gitlab.com:logmancuso/linuxvm_profile.git `


```
mv linuxvm_profile ~/.linuxvm_profile
# add this to .bashrc
if [ -d ~/.linuxvm_profile ]; then
	source ~/.linuxvm_profile/bash_aliases.sh
	GIT_PROMPT_ONLY_IN_REPO=1
	source ~/.linuxvm_profile/bash-git-prompt/gitprompt.sh
fi
```

then run `exec bash`