###
# custom bash profile
# 
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 08-22-2019--14:21:26
###

# set default text editor
export EDITOR="nano"

# Print Message:
export PS1="\[\033[01;35m\]{\u@\H}\[\033[01;32m\]/\W\[\033[01;36m\]\[\033[00m\] > "

alias huawei_duplicate="xrandr --output eDP-1 --output DP-2 --same-as eDP-1"
alias bumble-nvidia="optirun -b none nvidia-settings -c :8"
alias browshrun="docker run -it --rm browsh/browsh $@"

###
# POSIX Aliases
###

# Anaconda Environments
alias source+="source activate $1"
alias souirce-="source deactivate"
alias conda+="conda activate $1"
alias conda-="conda deactivate"

#'ls' helpers
alias la="ls -l1hA"
alias lc="ls -1"

#'cd' helpers
alias cs..="cs ../"
alias fhere="find . -name $@"
alias cls="clear"

#'git' helpers
alias gpull="git pull $@"
alias gpush="git push $@"
alias gstatus="git status $@"
alias gcommit="git commit --all --gpg-sign $@"
alias gcheckout="git checkout $@"
alias gbranch="git branch $@"
alias gadd="git add $@"
alias gclone="git clone $@"
alias gdiff="git diff $@"
alias gmerge="git merge $@"
alias gclone="git clone $@"
alias grm="git rm $@"

# sys admin
alias whatsmyip="ifconfig | grep --color=always 'inet'"
alias mount="mount |column -t"
alias fastping="ping -c 100 -s.2 $1"
alias ports="netstat -tulanp"

# sshfs mount points for hyperion
# alias mountwork="sshfs hyperion:/work/ ~/Documents/Hyperion/cluster_work/ -o nonempty"
# alias mounthome="sshfs hyperion:/home/lmancuso ~/Documents/Hyperion/cluster_home/ -o nonempty" 

###
# update linux
# Fetches the list of available updates
# downloads the new updates
# Installs the new updates to distro
# removes old packages
###

function update(){
	# Get os name via uname
	declare -A osInfo;
	osInfo[/etc/debian_version]=apt-get
	osInfo[/etc/redhat-release]=yum
	osInfo[/etc/arch-release]=pacman
	for f in ${!osInfo[@]}
	do
		if [[ -f $f ]];then
			echo "Updating Using Package Manager: '${osInfo[$f]}' "
			case ${osInfo[$f]} in
				apt-get)
					sudo apt-get update 
					sudo apt-get upgrade
					sudo apt-get dist-upgrade 
					sudo apt-get auto-remove
					break	
				;;
				yum)
					sudo yum update
					sudo yum upgrade
					sudo yum autoremove
					sudo dnf upgrade # check for kernel update
					sudo dnf remove $(dnf repoquery --installonly --latest-limit=-1 -q) # remove all but newest kernel
					break
				;;
				pacman)
					sudo pacman -Syy
					sudo pacman -Su
					break
				;;
				*)
					echo "ERROR Cannot Find Package Manager"
				;;
			esac
		fi
	done
}

function cs() {
	cd "$@" && ls
}

function delete() {
	for item in $@
	do 
		mv $item ~/.recylebin/
	done
}

# change the date on a file if it has "Last Edit Date: string"
function newdate() {
	today_=$(date +%m-%d-%Y--%H:%M:%S)
	to_replace_="Last Edit Date:"
	for passed_ in $@; do 
		sed -i "0,\|.* $to_replace_.*|{s|$to_replace_.*|$to_replace_ $today_|g1}" $passed_
		echo "--- $passed_ --- $to_replace_ $today_"
	done
}

# change the date on a file if it has "Last Edit Date: string"
function gitdate() {
	today_=$(date +%m-%d-%Y--%H:%M:%S)
	to_replace_="Last Edit Date:"
	git_list=$(git show --pretty="" --name-only)
	for passed_ in ${git_list}; do 
		temp=$(echo ${passed_} | cut -d'/' -f2-)
		sed -i "0,\|.* $to_replace_.*|{s|$to_replace_.*|$to_replace_ $today_|g1}" ${temp}
		echo "--- ${temp} --- $to_replace_ $today_"
	done
}

# extract file type
function extract() {
	file_name_=$1
	if [ -z "$file_name_" ]; then
		# display usage if no parameters given
		echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
	else
		if [ -f $file_name_ ] ; then
			# NAME=${1%.*}
			# mkdir $NAME && cd $NAME
			case $file_name_ in
				*.tar.bz2)   tar xvjf $file_name_    ;;
				*.tar.gz)    tar xvzf $file_name_    ;;
				*.tar.xz)    tar xvJf $file_name_    ;;
				*.lzma)      unlzma $file_name_      ;;
				*.bz2)       bunzip2 $file_name_     ;;
				*.rar)       unrar x -ad $file_name_ ;;
				*.gz)        gunzip $file_name_      ;;
				*.tar)       tar xvf $file_name_     ;;
				*.tbz2)      tar xvjf $file_name_    ;;
				*.tgz)       tar xvzf $file_name_    ;;
				*.zip)       unzip $file_name_       ;;
				*.Z)         uncompress $file_name_  ;;
				*.7z)        7z x $file_name_        ;;
				*.xz)        unxz $file_name_        ;;
				*.exe)       cabextract $file_name_  ;;
				*)           echo "extract: '$file_name_' - unknown archive method" ;;
			esac
		else
			echo "$file_name_ - file does not exist"
		fi
	fi
}

###
# Creating a new file for a Program Command
###
function newfile() {
	clear
	# variable
	file_name_=$1
	template="/**
	* '$file_name_'
	*
	* Author/CopyRight: Mancuso, Logan
	* Last Edit Date:
	*
 **/

 /**
	* End '$file_name_'
 **/"
	# real computation below this line
	# if invalid name supplied
	if [ -f "$file_name_" ]; then
		echo $FILE_EXISTS
		return
	fi
	echo "$template" > $file_name_
	newdate $file_name_ #append new date
	echo "File Created"
}
